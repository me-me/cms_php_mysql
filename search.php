<?php include "admin/includes/functions.php" ?>
<!-- Header -->
<?php include "includes/header.php" ?>
<!-- End header -->
    <!-- Navigatio -->
    <?php include "includes/navigation.php" ?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
              <?php searchBlog($connect) ; ?>
            <!-- Pager -->
            <ul class="pager">
          <?php
          $num = countRow($connect);
          $count = ceil($num / 5);
          $i = 0;
          if (isset($_GET['id'])){
            $id = $_GET['id'];
          if (isset($_GET['rl'])){
            $rl = $_GET['rl'];
          while ($i++ < $count)
            echo "<li><a href='index.php?rl=$rl&&id={$id}&&u_id={$id_online}&&p_id={$post_id}&&hellotherethisisme=0123456789&&page=$i'>$i</a></li>";
          }
          } else {
            while ($i++ < $count)
          echo "<li><a href='index.php?page=$i'>$i</a></li>";
          }
          ?>
            </ul>
        </div>
            <!-- Blog Sidebar Widgets Column -->
            <?php include "includes/sidebar.php" ?>
        </div>
        <!-- /.row -->
        <hr>
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->
<!-- footer -->
<?php include "includes/footer.php" ?>
<!-- End footer -->
