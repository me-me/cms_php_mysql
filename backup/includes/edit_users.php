<?php include "../includes/db.php" ?>


<?php
if (isset($_GET['user_id']))
{
  $p_id = $_GET['user_id'];
  $query = "SELECT * FROM USERS WHERE user_id={$p_id} ";
  $rest = mysqli_query($connect, $query);
  if (!$rest){
    die ('Failed to query ' . mysqli_error($connect));
  } else {
    while ($row = mysqli_fetch_assoc($rest)){
      $username = $row['username'];
      $username = filter_var($username, FILTER_SANITIZE_STRING);
      $password = $row['password'];
      $password = filter_var($password, FILTER_SANITIZE_STRING);
      $firstname = $row['firstname'];
      $firstname = filter_var($firstname, FILTER_SANITIZE_STRING);
      $lastname = $row['lastname'];
      $lastname = filter_var($lastname, FILTER_SANITIZE_STRING);
      $p_image = $row['image'];
      $role = $row['role'];
      $role = filter_var($role, FILTER_SANITIZE_STRING);
    }
  }
}
  if (isset($_POST['update_user'])){
    $user_id = $_GET['user_id'];
    $username = $_POST['username'];
    $username = filter_var($username, FILTER_SANITIZE_STRING);
    $password = $_POST['password'];
    $password = crypt($password, '$12cA6./@^$some crazystring');
    $firstname = $_POST['firstname'];
    $firstname = filter_var($firstname, FILTER_SANITIZE_STRING);
    $lastname = $_POST['lastname'];
    $lastname = filter_var($lastname, FILTER_SANITIZE_STRING);
    $p_img = $_FILES['image']['name'];
    $p_img_dir = $_FILES['image']['tmp_name'];
    $role = $_POST['role'];
    $role = filter_var($role, FILTER_SANITIZE_STRING);
    if(is_dir('images/')) {
    move_uploaded_file($p_img_dir, "../image/$p_img");
    } else {
    echo 'Le dossier n\'existe pas';
    }
   $query_p_insert = "UPDATE USERS SET username='{$username}', user_password='{$password}', firstname='{$firstname}', lastname='{$lastname}', image='../image/$p_img', role='{$role}', randSalt='{$randsalt}' ";
   $query_p_insert .= "WHERE user_id={$user_id} ";
   $res_p_insert = mysqli_query($connect, $query_p_insert);
   if (!$res_p_insert){
     die ('Failed to query' . mysqli_error($connect));
     $valid = 0;
   } else
   $valid = 1;
 }
 ?>
 <?php if ($valid == 1)
  echo "<p style='color:green'><b>Users has been updatted successfully</b> <a href='users.php'> View users</a></p>";

?>
 <form action="" method="post" enctype="multipart/form-data">
   <div class="form-group">
     <label for="post_author">Username</labl>
     <input type="text" class="form-control" name="username" value="<?php echo $username ?>" required>
   </div>

   <div class="form-group">
     <label for="post_stats">Password</labl>
     <input type="password" class="form-control" name="password" value="<?php echo $password ?>" required>
   </div>

   <div class="form-group">
     <label for="post_tags">Firstname</labl>
     <input type="text" class="form-control" name="firstname" value="<?php echo $firstname ?>">
   </div>

   <div class="form-group">
     <label for="post_tags">Lastname</labl>
     <input type="text" class="form-control" name="lastname" value="<?php echo $lastname ?>">
   </div>

   <div class="form-group">
     <label for="post_img">image</labl><br>
     <img style="width:100px;" src="<?php echo "../image/$p_image" ?>" /><br>
     <br>
     <input type="file"  value="<?php echo $p_image ; ?>" name="image">
   </div>

   <div  class="form-group">
     <label for="sel2">Role</labl>
       <select  name="role" class="form-control" id="sel2" value="<?php echo $role ?>">
           <?php
            if (empty($role) || $role != 'Admin' || $role != 'Subscriber' ){
              $role='Admin';
              echo "<option value='{$role}'>$role</option>";
            };
            if ($role == 'Subscriber'){
              echo "<option value='Subscriber'>Suscriber</option>";
             echo "<option value='Admin'>Admin</option>";
           } else
           echo "<option value='Subscriber'>Subscriber</option>";
            ?>
         </select>
   </div>

   <div class="form-group">
     <input type="submit" class="btn btn-success" name="update_user" value="Update user">
     <button class="btn btn-primary" href=""><a href="users.php" style="color:white; text-decoration:none;">Cancel</a></button>
   </div>
 </form>
