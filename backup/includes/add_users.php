<?php include "../includes/db.php" ?>
<?php
  function checkConnect($res){
    if (!$res){
      die (';Failed to query' . mysqli_error($connect));
    }
  }
?>

<?php
  if (isset($_POST['add'])){
    $username = $_POST['username'];
    $password = $_POST['password'];
    $username = mysqli_real_escape_string($connect, $username);
    $password = mysqli_real_escape_string($connect, $password);
    $password = crypt($password, '$12cA6./@^$some crazystring');
    $Firstname = $_POST['firstname'];
    $email = $_POST['email'];
    $lastname = $_POST['lastname'];
    $image = $_FILES['image']['name'];
    $p_img_dir = $_FILES['image']['tmp_name'];
    $role = $_POST['role'];
    $ranSand = $_POST['randSand'];
    if(is_dir('images/')) {
    move_uploaded_file($p_img_dir, "../image/$image");
    } else {
    echo 'Le dossier n\'existe pas';
    }
  $query_p_insert = "INSERT INTO USERS(username, user_password, email, firstname, lastname, image, role, randSalt ) ";
   $query_p_insert .= "VALUES ( '{$username}', '{$password}', '{$Firstname}', '{$email}', '{$lastname}', '{$image}', '{$role}', '{$ranSand}') ";
   $res_p_insert = mysqli_query($connect, $query_p_insert);
   if (!$res_p_insert){
     die ('Failed to query add users' . mysqli_error($connect));
     $valid = 0;
   } else
    $valid = 1;
 }

 ?>
 <?php if ($valid == 1)
 echo "<p style='color:green'><b>Users has been added successfully</b> <a href='users.php'> View users</a></p>";
?>

<form action="" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="post_author">Username</labl>
    <input type="text" class="form-control" name="username" required>
  </div>

  <div class="form-group">
    <label for="post_stats">Password</labl>
    <input type="password" class="form-control" name="password" required>
  </div>

  <div class="form-group">
    <label for="post_tags">Email</labl>
    <input type="text" class="form-control" name="email" required>
  </div>

  <div class="form-group">
    <label for="post_tags">Firstname</labl>
    <input type="text" class="form-control" name="firstname">
  </div>

  <div class="form-group">
    <label for="post_tags">Lastname</labl>
    <input type="text" class="form-control" name="lastname">
  </div>

  <div class="form-group">
    <label for="post_img">Image</labl>
    <input type="file"  name="image">
  </div>

  <div  class="form-group">
    <label for="sel2">Role</labl>
      <select name="role" class="form-control" id="sel2" required>
          <option selected>Select option</option>
          <option value="Admin" >Admin</option>
          <option value="Subscriber" >Subscriber</option>
        </select >
  </div>


  <div class="form-group">
    <input type="submit" id="add user" class="btn btn-success" name="add" value="Add user">
    <button class="btn btn-primary" href=""><a href="users.php" style="color:white; text-decoration:none;">Cancel</a></button>
  </div>
</form>
