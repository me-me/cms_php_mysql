<?php function redirPost(){ header('location: posts.php'); }; ?>
<?php include "../includes/db.php" ?>
<?php
  function checkConnect($res){
    if (!$res){
      die (';Failed to query' . mysqli_error($connect));
    }
  }
?>

<?php
function cleanMe($input) {
   $input = htmlspecialchars($input, ENT_IGNORE, 'utf-8');
   $input = strip_tags($input);
   $input = stripslashes($input);
   return $input;
}

 ?>

<?php
  if (isset($_POST['create'])){
    $p_user_id = $_SESSION['user_id'];
    $p_title = $_POST['title'];
    $p_title = filter_var($p_title, FILTER_SANITIZE_STRING);
    $p_cat = $_POST['post_category_id'];
    $p_cat = filter_var($p_cat, FILTER_SANITIZE_STRING);
    $query = "SELECT cat_id FROM category WHERE cat_title='{$p_cat}' ";
    $res_id = mysqli_query($connect, $query);
    $row = mysqli_fetch_assoc($res_id);
    $p_cat_id = $row['cat_id'];
    $p_author = $_POST['post_author'];
    $p_author = filter_var($p_author, FILTER_SANITIZE_STRING);
    $p_status = $_POST['post_status'];
    $p_status = filter_var($p_status, FILTER_SANITIZE_STRING);
    $p_img = $_FILES['post_img']['name'];
    $p_img_dir = $_FILES['post_img']['tmp_name'];
    $p_tags = $_POST['post_tags'];
    $p_tags = filter_var($p_tags, FILTER_SANITIZE_STRING);
    $p_content = $_POST['post_content'];
    $p_content = utf8_encode($p_content);
    $p_content = chunk_split($p_content, 100, '\n');
    //$p_content = str_replace(' ', '-', $p_content)
    //$p_content = htmlentities($p_content);
    //$p_content = filter_var($p_content, FILTER_SANITIZE_SPECIAL_CHARS);
    //$p_content =  htmlspecialchars($p_content);
    //$p_content = htmlentities($p_content);
    $p_content = filter_var($p_content, FILTER_SANITIZE_STRING);
    preg_replace('/[^A-Za-z0-9\-]/', '', $p_content);
    //$p_content = str_replace('/r', ' ', $p_content);
    $p_date = date('d-m-y');
    $p_comments_count = 4;
    $p_views_count = 4;
    if(is_dir('images/')) {
    move_uploaded_file($p_content, "../image/$p_img");
    } else {
    echo 'Le dossier n\'existe pas';
    }
  $query_p_insert = "INSERT INTO posts(post_category_id, post_user_id, post_title, post_author, post_date, post_image, post_content, post_tags, post_status, post_comment_count, post_views_count) ";
   $query_p_insert .= "VALUES ({$p_cat_id}, {$p_user_id}, '{$p_title}', '{$p_author}', now(), '{$p_img}', '{$p_content}', '{$p_tags}', '{$p_status}', {$p_comments_count}, {$p_views_count} )";
   $res_p_insert = mysqli_query($connect, $query_p_insert);
   if (!$res_p_insert){
     die ('Failed to query add posts' . mysqli_error($connect));
     $valid = 0 ;
   } else
    $valid = 1;
  }
 ?>
 <?php if ($valid == 1)
  echo "<p style='color:green'><b>Post has been added successfully</b> <a href='posts.php'> View posts</a></p>";
?>
<form action="" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="title">Post title</labl>
    <input type="text" class="form-control" name="title" >
  </div>

  <div class="form-group">
    <label for="post_category">Post category</labl><br>
    <select value="" name="post_category_id">
      <?php
        $query = "SELECT * FROM category ";
        $res = mysqli_query($connect, $query);
        while ($row = mysqli_fetch_assoc($res)){
          $cat_title = $row['cat_title'];
          echo "<option value='{$cat_title}'>{$cat_title}</option>";
        }
      ?>
    </select>
  </div>

  <div class="form-group">
    <label for="post_author">Post author</labl>
    <input type="text" class="form-control" name="post_author" >
  </div>

  <div class="form-group">
    <label>Post status</label><br>
    <select value="" name="post_status">
        <option value="Draft">Draft</option>;
        <option value="Published">Published</option>;
    </select>
  </div>

  <div class="form-group">
    <label for="post_img">Post image</labl>
    <input type="file"  name="post_img" >
  </div>

  <div class="form-group">
    <label for="post_tags">Post tags</labl>
    <input type="text" class="form-control" name="post_tags" >
  </div>

  <div class="form-group">
    <label for="Post content">Post content</labl>
    <textarea type="text" class="form-control"  rows=20 cols="40" name="post_content" required> </textarea>
  </div>

  <div class="form-group">
    <input type="submit" class="btn btn-success" name="create" value="Publish posts">
    <button class="btn btn-primary" href=""><a href="posts.php" style="color:white; text-decoration:none;">Cancel</a></button>
  </div>
</form>
