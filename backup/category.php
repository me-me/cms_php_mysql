<?php include "includes/header.php" ?>
<?php include "includes/functions.php"  ?>



<div id="wrapper">
<?php include "includes/navigation.php" ?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">
          Welcome
          <?php showName($connect); ?>
        </h1>
        <div class="col-lg-6 col-sm-6 col-xs-6">
            <form action=""  method="post">
              <div class="group-form">
                <?php check_input_field($connect);?>
                <label for="cat_title">Add category</label>
                <input class="form-control" type="text" name="cat_title">
              </div><br>
                <div class="group-form">
                  <input type="submit" class="btn btn-success" name="submit" value="Add category">
                  <input type="submit" class="btn btn-danger" name="Delete_all_cat" value="Delete all">
                  </div>
            </form>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-6">
          <table class="table table-bordereds">
          <thead>
            <tr class="success">
              <th>Id</th>
              <th>Category title</th>
            </tr>
          </thead>
          <tbody>
            <?php showCategory($connect); ?>
            <?php deleteCategory($connect);?>
            <?php DelAllCat($connect); ?>
          </tbody>
          </table>
        </div><br>
        <div class="col-lg-6 col-sm-6 col-xs-6" >

        <form action="update_cat.php"  method="post">
          <?php getEdit($connect); ?>
        </form>

      </div>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
    <!-- /#wrapper -->

<?php include "includes/footer.php" ?>
