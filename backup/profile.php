<?php include "includes/header.php" ?>
<?php include "includes/functions.php"  ?>
<div id="wrapper">
<?php include "includes/navigation.php" ?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">
          Welcome to <?php showName($connect); ?>
      </h1>

      <?php
      if (isset($_SESSION['role']))
      {
        $p_id = $_SESSION['user_id'];
        $query = "SELECT * FROM USERS WHERE user_id='{$p_id}'";
        $rest = mysqli_query($connect, $query);
        if (!$rest){
          die ('Failed to query ' . mysqli_error($connect));
        } else {
          while ($row = mysqli_fetch_assoc($rest)){
            $username = $row['username'];
            $password = $row['password'];
            $firstname = $row['firstname'];
            $lastname = $row['lastname'];
            $p_image = $row['image'];
            $role = $row['role'];
            $randSalt = $row['randSalt'];
          }
        }
      }
      if (isset($_POST['Save_profile'])){
        $user_id = $_SESSION['user_id'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $password = crypt($password. '$12cA6./@^$some crazystring');
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $p_img = $_FILES['image']['name'];
        $p_img_dir = $_FILES['image']['tmp_name'];
        $role = $_POST['role'];
        $randSalt = $_POST['randSalt'];
        if(is_dir('images/')) {
        move_uploaded_file($p_img_dir, "../image/$p_img");
        } else {
        echo 'Le dossier n\'existe pas';
        }
       $query_p_insert = "UPDATE USERS SET username='{$username}', user_password='{$password}', firstname='{$firstname}', lastname='{$lastname}', image='../image/$p_img', role='{$role}', randSalt='{$randsalt}' ";
       $query_p_insert .= "WHERE user_id={$user_id} ";
       $res_p_insert = mysqli_query($connect, $query_p_insert);
         if (!$res_p_insert){
           die ('Failed to query' . mysqli_error($connect));
         }
         echo "<script>
        window.location.href= 'users.php' ;
        </script>";
        }
       ?>
       <form action="" method="post" enctype="multipart/form-data">
         <div class="form-group">
           <label for="post_author">Username</labl>
           <input type="text" class="form-control" name="username" value="<?php echo $username ?>" required>
         </div>

         <div class="form-group">
           <label for="post_stats">Password</labl>
           <input type="password" class="form-control" name="password" value="<?php echo $password ?>" required>
         </div>

         <div class="form-group">
           <label for="post_tags">Firstname</labl>
           <input type="text" class="form-control" name="firstname" value="<?php echo $firstname ?>">
         </div>

         <div class="form-group">
           <label for="post_tags">Lastname</labl>
           <input type="text" class="form-control" name="lastname" value="<?php echo $lastname ?>">
         </div>

         <div class="form-group">
           <label for="post_img">image</labl><br>
           <img style="width:100px;" src="<?php echo "../image/$p_image" ?>" /><br>
           <br>
           <input type="file"  value="<?php echo $p_image ; ?>" name="image">
         </div>



         <?php if (isset($_SESSION['role'])){
          $role = $_SESSION['role'];
          if ($role == 'Admin'){

          echo "<div  class='form-group'>
           <label for='sel2'>Role</labl>
             <select  class='form-control' id='sel2' value='";
             echo $role;
             echo ">";
                  if (empty($role) || $role != 'Admin' || $role != 'Subscriber' ){
                    $role='Admin';
                    echo "<option name='role'>$role</option>";
                  };
                  if ($role == 'Subscriber'){
                    echo "<option name='role'>Suscriber</option>";
                   echo "<option name='role'>Admin</option>";
                 } else
                 echo "<option name='role'>Subscriber</option>";
            echo "</select></div>";
          };
       };
       ?>





         <div class="form-group">
           <input type="submit" class="btn btn-success" name="Save_profile" value="Save profile">
         </div>
       </form>
    </div>
    </div>
  </div>
</div>
</div>
    <!-- /#wrapper -->

<?php include "includes/footer.php" ?>
