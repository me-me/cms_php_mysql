<?php include "includes/header.php" ?>
<div id="wrapper">
<?php include "includes/navigation.php" ?>
<?php include "includes/functions.php"  ?>

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">
          Welcome to <?php showName($connect); ?>
        </h1>
        <div class="col-lg-6 col-sm-6 col-xs-6">
            <form action=""  method="post">
              <div class="group-form">
                <?php check_input_field($connect);?>
                <label for="cat_title">Add category</label>
                <input class="form-control" type="text" name="cat_title">
              </div><br>
                <div class="group-form">
                  <input type="submit" class="btn btn-success" name="submit" value="Add category">
                  </div>
            </form>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-6">
          <table class="table table-bordered">
          <thead>
            <tr>
              <th>Id</th>
              <th>Category title</th>
            </tr>
          </thead>
          <tbody>
            <?php showCategory($connect); ?>
            <?php deleteCategory($connect);?>
          </tbody>
          </table>
        </div><br>
        <div class="col-lg-6 col-sm-6 col-xs-6" >
        <form action="category.php"  method="post">
          <?php getEdit($connect); ?>
        </form>
        <?php updateCat($connect); ?>
      </div>

      </div>
    </div>
    </div>
  </div>
</div>
<?php include "includes/footer.php" ?>
