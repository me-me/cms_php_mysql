<div class="col-md-4">

    <!-- Blog Search Well -->
    <div class="well">
        <h4>Blog Search</h4>
        <div class="input-group">
          <form <?php if (isset($_GET['id'])){ $id = $_GET['id']; $rl=$_GET['rl'];
            echo "action='search.php?rl=$rl&&id=$id&&hellotherethisisme=0123456789' "; }
            else
            echo "action='search.php' "; ?>
            method="post">
            <span class="input-group-btn">
                <input name="search" type="text" class="form-control">
                <button name="submit" class="btn btn-default" type="submit">
                    <span class="glyphicon glyphicon-search"></span>
            </button>
            </span>
          </form>
        </div>
        <!-- /.input-group -->
    </div>
    <!-- Login section -->

    <div class="well">
        <h4>Login</h4>
          <form action="includes/login.php" method="post">
            <div  class="input-group">
            <input name="username" type="text" class="form-control" placeholder="Enter username" required>
            <div>
            <input name="password" type="password" class="form-control" placeholder="Enter password" required>
            <span class="input-group-btn">
                <button name="login" class="btn btn-info" type="submit">Sign in
                    <span class="glyphicon glyphicon-user"></span>
            </button>
            </span>
          </div>
          </div>
          </form>
        </div>

    <!-- Blog Categories Well -->
    <div class="well">
        <h4>Blog Categories</h4>

        <div class="row">
            <div class="col-lg-6">
                <ul class="list-unstyled">
                  <?php
                  $query_cat = "SELECT * FROM category ";
                  $rest_cat = mysqli_query($connect, $query_cat);
                  if (!$rest_cat){
                    die ('Error invalid query' . mysqli_error($rest_cat));
                  } else {
                    if (isset($_GET['id'])){
                      $id = $_GET['id'];
                    if (isset($_GET['rl'])){
                      $rl = $_GET['rl'];
                    while ($row = mysqli_fetch_assoc($rest_cat)){
                      $cat_name = $row['cat_title'];
                      $cat_id = $row['cat_id'];
                      echo "<li><a href='index.php?rl=$rl&&id={$id}&&u_id={$id_online}&&p_id={$post_id}&&hellotherethisisme=0123456789&&cat_id={$cat_id}'>{$cat_name}</a></li>";
                    }
                  }
                } else
                while ($row = mysqli_fetch_assoc($rest_cat)){
                  $cat_name = $row['cat_title'];
                  $cat_id = $row['cat_id'];
                  echo "<li><a href='index.php?cat_id={$cat_id}'>{$cat_name}</a></li>";
                }
              }
                  ?>
                </ul>
            </div>
              </div>
        <!-- /.row -->
    </div>
    <!-- Side Widget Well -->
    <div class="well">
        <h4>Book of the day</h4>
        <p>Think and Grow Rich was written in 1937 by Napoleon Hill, promoted as a personal development and self-improvement book. Hill writes that he was inspired by a suggestion from business magnate and later-philanthropist Andrew Carnegie.[1] While the book's title and much of the text concerns increased income, the author insists that the philosophy taught in the book can help people succeed in any line of work, to do and be anything they can imagine.</p>
    </div>
</div>
