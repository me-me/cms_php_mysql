<?php
global $connect;

$servername = "localhost";
$username = "root";
$password = "toor";

$user_pass = 'admin';
$user_pass = crypt($user_pass, '$12cA6./@^$some crazystring');

// Create connection
$conn = mysqli_connect($servername, $username, $password);

// Check connection
if (!$conn) {
    die("Connection failed starting: " . mysqli_connect_error());
}

// Check if databse cms exsits
$query = "SHOW DATABASES LIKE 'cms' ";
$result = mysqli_query($conn, $query);
if( mysqli_num_rows($result) ) {
    $db = 'cms' ;
    $connect = mysqli_connect($servername, $username, $password, $db);
} else {

  //if database cms does not exist then creat it
  $query_create_db = "CREATE DATABASE cms ";
  $res = mysqli_query($conn, $query_create_db);
  if (!$res) {
      die("Query failed: Create db " . mysqli_error($con));
  } else {

    // Connect to the database
    $db = 'cms';
    $connect = mysqli_connect($servername, $username, $password, 'cms');
    if (!$connect) {
        die("Connection failed: connect to db " . mysqli_connect_error());
    }

    // Create tables
    $query_cat = "CREATE TABLE `category` (
      `cat_id` int(11) NOT NULL,
      `cat_title` varchar(255) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1; ";

    $query_comment = "CREATE TABLE `comments` (
  `comment_id` int(3) NOT NULL,
  `comment_user_id` int(11) NOT NULL,
  `comment_post_id` int(3) NOT NULL,
  `comment_author` varchar(255) NOT NULL,
  `comment_email` varchar(255) NOT NULL,
  `comment_content` longtext NOT NULL,
  `comment_status` varchar(255) NOT NULL,
  `comment_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; ";

    $query_posts = "CREATE TABLE `posts` (
  `post_id` int(3) NOT NULL,
  `post_user_id` int(11) NOT NULL,
  `post_category_id` int(11) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_author` varchar(255) NOT NULL,
  `post_status` varchar(255) NOT NULL,
  `post_date` date NOT NULL,
  `post_image` varchar(255) NOT NULL,
  `post_content` text NOT NULL,
  `post_tags` varchar(255) NOT NULL,
  `post_comment_count` int(11) NOT NULL,
  `post_views_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; ";

    $query_users = "CREATE TABLE `USERS` (
  `user_id` int(3) NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL DEFAULT 'user',
  `lastname` varchar(255) NOT NULL DEFAULT 'user',
  `image` text,
  `role` varchar(255) NOT NULL,
  `Online` varchar(255) NOT NULL DEFAULT 'offline',
  `randSalt` varchar(255) NOT NULL DEFAULT '$12cA6./@^$some crazystring'
) ENGINE=InnoDB DEFAULT CHARSET=latin1; ";



$query_online_users ="CREATE TABLE `user_online` (
  `id_session` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `session` varchar(255) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; ";

// Create online users table
$res = mysqli_query($connect, $query_online_users);
if (!$res){
  die ('Failed to craete category table ' . mysqli_error($connect));
}

$query = "ALTER TABLE `user_online`
  ADD PRIMARY KEY (`id_session`); ";

$res = mysqli_query($connect, $query);
if (!$res){
  die ('Failed to alter table ' . mysqli_error($connect));
}


$query = "ALTER TABLE `user_online`
  MODIFY `id_session` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3; ";

$res = mysqli_query($connect, $query);
if (!$res){
  die ('Failed to alter table ' . mysqli_error($connect));
}

// Create category table
    $res = mysqli_query($connect, $query_cat);
    if (!$res){
      die ('Failed to craete category table ' . mysqli_error($connect));
    }


    $query = "ALTER TABLE `category`
    ADD PRIMARY KEY (`cat_id`); ";

    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to alter table ' . mysqli_error($connect));
    }


    $query = "ALTER TABLE `category`
  MODIFY `cat_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19; ";

    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to alter table ' . mysqli_error($connect));
    }



// Create comments table
    $res_com = mysqli_query($connect, $query_comment);
    if (!$res){
      die ('Failed to craete comments table ' . mysqli_error($connect));
    }

    $query = "ALTER TABLE `comments`
    ADD PRIMARY KEY (`comment_id`); ";

    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to AlTER table ' . mysqli_error($connect));
    }

    $query = "ALTER TABLE `comments`
  MODIFY `comment_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44; ";

    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to ALTER table ' . mysqli_error($connect));
    }


//Create Posts table------------------
    $res_post = mysqli_query($connect, $query_posts);
    if (!$res){
      die ('Failed to craete posts table ' . mysqli_error($connect));
    }

    $query = "ALTER TABLE `posts`
    ADD PRIMARY KEY (`post_id`); ";

    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to AKTER table ' . mysqli_error($connect));
    }

    $query = "ALTER TABLE `posts`
  MODIFY `post_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3; ";

    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to ALTER table ' . mysqli_error($connect));
    }
//-----------------------------------


// Create users table ----------------------------
    $res_user = mysqli_query($connect, $query_users);
    if (!$res){
      die ('Failed to craete user table ' . mysqli_error($connect));
    }

    $query = "ALTER TABLE `USERS`
    ADD PRIMARY KEY (`user_id`); ";

    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to alter table ' . mysqli_error($connect));
    }

    $query = "ALTER TABLE `USERS`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152; ";

    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to alter table ' . mysqli_error($connect));
    }

// Insert Admin to users table
    $query = "INSERT INTO USERS(username, user_password, firstname, lastname, email, image, role, randSalt) ";
    $query .= "VALUES ('admin', '{$user_pass}', 'admin', 'admin', 'admin@admin.com', 'image', 'Admin', '2') ";
    $res_add_admin = mysqli_query($connect, $query);
    if (!$res_add_admin){
      die ('Failed to add user ' . mysqli_error($connect));
    }
  }
}
?>
