<?php include "includes/header.php" ?>
<?php include "includes/functions.php"  ?>

<div id="wrapper">
<?php include "includes/navigation.php" ?>
<div id="page-wrapper">
                <!-- /.row -->
<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-file-text fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                  <div class='huge'><?php countPost($connect); ?></div>
                        <div>Posts</div>
                    </div>
                </div>
            </div>
            <a href="posts.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                     <div class='huge'><?php countComments($connect); ?></div>
                      <div>Comments</div>
                    </div>
                </div>
            </div>
            <a href="comments.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <?php if (isset($_SESSION['role'])){
    $user_role = $_SESSION['role'];
    if ($user_role == 'Admin'){
    echo "<div class='col-lg-3 col-md-6'>
        <div class='panel panel-yellow'>
            <div class='panel-heading'>
                <div class='row'>
                    <div class='col-xs-3'>
                        <i class='fa fa-user fa-5x'></i>
                    </div>
                    <div class='col-xs-9 text-right'>
                    <div class='huge'>"; countUsers($connect);
                    echo "</div>
                        <div> Users</div>
                    </div>
                </div>
            </div>
            <a href='users.php'>
                <div class='panel-footer'>
                    <span class='pull-left'>View Details</span>
                    <span class='pull-right'><i class='fa fa-arrow-circle-right'></i></span>
                    <div class='clearfix'></div>
                </div>
            </a>
        </div>
    </div>";

    echo "<div class='col-lg-3 col-md-6'>
        <div class='panel panel-red'>
            <div class='panel-heading'>
                <div class='row'>
                    <div class='col-xs-3'>
                        <i class='fa fa-list fa-5x'></i>
                    </div>
                    <div class='col-xs-9 text-right'>
                        <div class='huge'>";
                        countCategory($connect);
                        echo "</div>
                         <div>Categories</div>
                    </div>
                </div>
            </div>
            <a href='categories.php'>
                <div class='panel-footer'>
                    <span class='pull-left'>View Details</span>
                    <span class='pull-right'><i class='fa fa-arrow-circle-right'></i></span>
                    <div class='clearfix'></div>
                </div>
            </a>
        </div>
    </div>
    </div>";
  };
};
?>
    <div class="row">
      <div class="col-lg-12 col-md-12">

      <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Posts', 'Category', 'Users', 'Cmments'],
          [ '<?php echo date("Y"); ?>', <?php countPost($connect); ?>, <?php countCategory($connect); ?>, <?php countUsers($connect); ?>, <?php countComments($connect); ?>]
      ]);

      var options = {
      chart: {
          title: 'Performances',
          subtitle: 'Posts, Coemments, categories and users on: <?php echo date('Y - m - d'); ?>' ,
        }
      };
      var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
      </script>
        <div  class="col-lg-12 col-sms-12 col-md-12" id="columnchart_material" style="height: 500px;"></div>
      </div>
    </div>
</div>
                <!-- /.row -->
</div>
<!-- /#wrapper -->
<?php include "includes/footer.php" ?>
