am<!-- Post Id -->
<?php
function postId($connect, $p_id){
    if (isset($_GET['p_id']))
    {
    $p_id = $_GET['p_id'];
    $query = "SELECT * FROM posts WHERE post_id={$p_id} ";
    $res = mysqli_query($connect, $query);
    if(!$res){
      die ('Failed to add post : ' . mysqli_error($connect));
    } else {
      while ($row = mysqli_fetch_assoc($res)){
        $row_title = $row['post_title'];
        $row_author = $row['post_author'];
        $row_date = $row['post_date'];
        $row_image = $row['post_image'];
        $row_content = $row['post_content'];
        $row_content = chunk_split($row_content, 150);
        echo "<h1><a href='#'>{$row_title}</a></h1>
          <p class='lead'>
              by <a href='#'>{$row_author}</a>
          </p>
          <hr>
          <p><span class='glyphicon glyphicon-time'></span> Check Posted on {$row_date}</p>
          <hr>
          <img class='img-responsive' src='image/{$row_image}' alt=''>
          <hr>
          <p>{$row_content}</p>
          <hr>";
      }
    }
  }
}
?>

<!-- Get category -->

<?php
function getCategory($connect, $cat_id){
  if (!(isset($_GET['p_id']))) {
  if (isset($_GET['cat_id']))
  {
    $p_id = $_GET['cat_id'];
    $query = "SELECT * FROM posts WHERE post_category_id={$p_id} ";
    $res = mysqli_query($connect, $query);
    if(!$res){
      die ('Failed to add post : ' . mysqli_error($connect));
    } else {
      while ($row = mysqli_fetch_assoc($res)){
        $row_title = $row['post_title'];
        $row_author = $row['post_author'];
        $row_date = $row['post_date'];
        $row_image = $row['post_image'];
        $row_content = $row['post_content'];
        $row_content = chunk_split($row_content, 150);
        echo "<h1><a href='#'>{$row_title}</a></h1>
          <p class='lead'>
              by <a href='#'>{$row_author}</a>
          </p>
          <hr>
          <p><span class='glyphicon glyphicon-time'></span> Posted on {$row_date}</p>
          <hr>
          <img class='img-responsive' src='image/{$row_image}' alt=''>
          <hr>
          <p>{$row_content}</p>
          <hr>";
      }
    }
  }
}
}
?>


<?php
function searchBlog($connect){
if (isset($_POST['submit'])){
  $search = $_POST['search'];
  $search = filter_var($search, FILTER_SANITIZE_STRING);
  $query = "SELECT * FROM posts WHERE post_tags LIKE '%$search%' ";
  $results = mysqli_query($connect, $query);

  if (!$results){
    die ('Failed to query'. mysqli_error($connect));
  } else{
      $count = mysqli_num_rows($results);
      if ($count == 0){
        echo "<h4>No results found</h4>";
      } else{
        echo "<h4>Some results : $count results. </h4> <br>";

        while ($row = mysqli_fetch_assoc($results)){
          $p_id = $row['post_id'];
          $row_title = $row['post_title'];
          $row_author = $row['post_author'];
          $row_date = $row['post_date'];
          $row_image = $row['post_image'];
          $row_content = $row['post_content'];
          $row_content = chunk_split($row_content, 150);
          if (isset($_GET['id']))
            $id = $_GET['id'];
          if (isset($_GET['rl'])){
            $rl = $_GET['rl'];
          echo "
          <h2>
              <a href='post.php?rl=$rl&&id={$id}&&u_id={$id_online}&&p_id={$post_id}&&hellotherethisisme=0123456789&&p_id={$p_id}'>{$row_title}</a>
          </h2>
          <p class='lead'>
              by <a>{$row_author}</a>
          </p>
          <p><span class='glyphicon glyphicon-time'></span> Posted on {$row_date}</p>
          <hr>
          <img class='img-responsive' src='image/{$row_image}'>
          <hr>
          <p>{$row_content}</p>
          <a class='btn btn-primary' href='post.php?rl=$rl&&id={$id}&&u_id={$id_online}&&p_id={$post_id}&&hellotherethisisme=0123456789&&p_id={$p_id}'>Read More <span class='glyphicon glyphicon-chevron-right'></span></a>

          <hr>  ";
        } else {
          echo "
          <h2>
              <a href='post.php?p_id={$p_id}'>{$row_title}</a>
          </h2>
          <p class='lead'>
              by <a>{$row_author}</a>
          </p>
          <p><span class='glyphicon glyphicon-time'></span> Posted on {$row_date}</p>
          <hr>
          <img class='img-responsive' src='image/{$row_image}'>
          <hr>
          <p>{$row_content}</p>
          <a class='btn btn-primary' href='post.php?p_id={$p_id}'>Read More <span class='glyphicon glyphicon-chevron-right'></span></a>

          <hr>  ";
        }
        }
      }
    }
  }
}
?>


<!-- Print posts -->
<?php
function addPost($connect, $page_1, $id, $rl){
    if (isset($_GET['id']))
      $id_online = $_GET['id'];
      if (isset($_GET['rl']))
        $rl = $_GET['rl'];
  $query = "SELECT * FROM posts WHERE post_status='Published' LIMIT $page_1, 5";
  $res = mysqli_query($connect, $query);
  while ($row = mysqli_fetch_assoc($res)){
    $post_id = $row['post_id'];
    $cat_id = $row['post_category_id'];
    $row_title = $row['post_title'];
    $row_author = $row['post_author'];
    $row_date = $row['post_date'];
    $row_image = $row['post_image'];
    $row_content = substr($row['post_content'],0,300);
    $p_content = utf8_encode($p_content);
    $row_content = chunk_split($row_content, 150);
    echo "
    <h2>
        <a href='post.php?rl=$rl&&id={$id}&&u_id={$id_online}&&p_id={$post_id}&&hellotherethisisme=0123456789'>{$row_title}</a>
    </h2>
    <p class='lead'>
        by <a href='index.php?rl=$rl&&id={$id}&&u_id={$id_online}&&p_id={$post_id}&&hellotherethisisme=0123456789'>{$row_author}</a>
    </p>
    <p><span class='glyphicon glyphicon-time'></span> Posted on {$row_date}</p>
    <hr>
    <img class='img-responsive' src='image/{$row_image}'>
    <hr>
    <p>{$row_content}...</p>
    <a class='btn btn-primary' href='post.php?rl=$rl&&id={$id}&&u_id={$id_online}&&p_id={$post_id}&&hellotherethisisme=0123456789'>Read More <span class='glyphicon glyphicon-chevron-right'></span></a>

    <hr>  ";
  }
}
?>


<!-- add post standard -->

<?php

function addPostStanStand($connect, $page_1){
  if (isset($_GET['rl'])){
    $rl = $_GET['rl'];
   if (isset($_GET['id'])){
     $id = $_GET['id'];
   if (isset($_GET['cat_id'])){
   $cat_id = $_GET['cat_id'];
$query = "SELECT * FROM posts WHERE post_status='Published' AND post_category_id={$cat_id} LIMIT $page_1, 5";
$res = mysqli_query($connect, $query);
while ($row = mysqli_fetch_assoc($res)){
  $post_id = $row['post_id'];
  $cat_id = $row['post_category_id'];
  $row_title = $row['post_title'];
  $row_author = $row['post_author'];
  $row_date = $row['post_date'];
  $row_image = $row['post_image'];
  $row_content = substr($row['post_content'],0,300);
  $p_content = utf8_encode($p_content);
  $row_content = chunk_split($row_content, 150);
    echo "
    <h2>
        <a href='post.php?rl=$rl&&u_id={$id_online}&&p_id={$post_id}&&hellotherethisisme=0123456789'>{$row_title}</a>
    </h2>
    <p class='lead'>
        by <a href='index.php?rl=$rl&&u_id={$id_online}&&p_id={$post_id}&&hellotherethisisme=0123456789''>{$row_author}</a>
    </p>
    <p><span class='glyphicon glyphicon-time'></span> Posted on {$row_date}</p>
    <hr>
    <img class='img-responsive' src='image/{$row_image}'>
    <hr>
    <p>{$row_content}...</p>
    <a class='btn btn-primary' href='post.php?rl=$rl&&u_id={$id_online}&&p_id={$post_id}&&hellotherethisisme=0123456789'>Read More <span class='glyphicon glyphicon-chevron-right'></span></a>

    <hr>  ";
  }
}
}
} else if (isset($_GET['cat_id'])){
 $cat_id = $_GET['cat_id'];
$query = "SELECT * FROM posts WHERE post_status='Published' AND post_category_id={$cat_id} LIMIT $page_1, 5";
$res = mysqli_query($connect, $query);
while ($row = mysqli_fetch_assoc($res)){
$post_id = $row['post_id'];
$cat_id = $row['post_category_id'];
$row_title = $row['post_title'];
$row_author = $row['post_author'];
$row_date = $row['post_date'];
$row_image = $row['post_image'];
$row_content = substr($row['post_content'],0,300);
$p_content = utf8_encode($p_content);
$row_content = chunk_split($row_content, 150);
  echo "
  <h2>
      <a href='post.php?p_id={$post_id}&&cat_id=$cat_id'>{$row_title}</a>
  </h2>
  <p class='lead'>
      by <a href='index.php?p_id={$post_id}&&cat_id=$cat_id'>{$row_author}</a>
  </p>
  <p><span class='glyphicon glyphicon-time'></span> Posted on {$row_date}</p>
  <hr>
  <img class='img-responsive' src='image/{$row_image}'>
  <hr>
  <p>{$row_content}...</p>
  <a class='btn btn-primary' href='post.php?p_id={$post_id}&&cat_id=$cat_id'>Read More <span class='glyphicon glyphicon-chevron-right'></span></a>

  <hr>  ";
}
}

if (isset($_GET['cat'])){
  $cat = $_GET['cat'];
  if ($cat == '1988'){
  $query = "SELECT * FROM posts WHERE post_status='Published' LIMIT $page_1, 5";
  $res = mysqli_query($connect, $query);
  if (!$res){
    die ('Failed to get post  ' . mysqli_error($connect));
  }
  while ($row = mysqli_fetch_assoc($res)){
  $post_id = $row['post_id'];
  $cat_id = $row['post_category_id'];
  $row_title = $row['post_title'];
  $row_author = $row['post_author'];
  $row_date = $row['post_date'];
  $row_image = $row['post_image'];
  $row_content = substr($row['post_content'],0,300);
  $p_content = utf8_encode($p_content);
  $row_content = chunk_split($row_content, 150);
  if (isset($_GET['rl'])){
    $rl = $_GET['rl'];
   if (isset($_GET['id'])){
     $id = $_GET['id'];
  echo "
  <h2>
      <a href='post.php?cat_id=$cat_id&&rl=$rl&&id={$id}&&p_id={$post_id}&&hellotherethisisme=0123456789'>{$row_title}</a>
  </h2>
  <p class='lead'>
      by <a href='index.php?cat_id=$cat_id&&rl=$rl&&id={$id}&&p_id={$post_id}&&hellotherethisisme=0123456789'>{$row_author}</a>
  </p>
  <p><span class='glyphicon glyphicon-time'></span> Posted on {$row_date}</p>
  <hr>
  <img class='img-responsive' src='image/{$row_image}'>
  <hr>
  <p>{$row_content}...</p>
  <a class='btn btn-primary' href='post.php?cat_id=$cat_id&&rl=$rl&&id={$id}&&p_id={$post_id}&&hellotherethisisme=0123456789'>Read More <span class='glyphicon glyphicon-chevron-right'></span></a>

  <hr>  ";
}
} else {
  echo "
  <h2>
      <a href='post.php?cat_id=$cat_id&&p_id={$post_id}'>{$row_title}</a>
  </h2>
  <p class='lead'>
      by <a href='index.php?cat_id=$cat_id&&p_id={$post_id}'>{$row_author}</a>
  </p>
  <p><span class='glyphicon glyphicon-time'></span> Posted on {$row_date}</p>
  <hr>
  <img class='img-responsive' src='image/{$row_image}'>
  <hr>
  <p>{$row_content}...</p>
  <a class='btn btn-primary' href='post.php?cat_id=$cat_id&&p_id={$post_id}'>Read More <span class='glyphicon glyphicon-chevron-right'></span></a>

  <hr>  ";
}
}
}
}
}
?>


<!-- Check the input if it is empty -->
<?php
function check_input_field($connect){
  if (isset($_POST['submit'])){
    $cat_title = $_POST['cat_title'];
    if ($cat_title == "" || empty($cat_title)){
      echo "This field should not be empty";
      echo "<br>";
    } else {
      $query_cat = "INSERT INTO category(cat_title) ";
      $query_cat .= "VALUES ('{$cat_title}') ";
      $rest_cat = mysqli_query($connect, $query_cat);
      if (!$query_cat){
        die ('Failed to execute the query' .  mysqli_error($rest_cat));
      }
    }
  }
}
 ?>

<!-- Show categories -->

<?php
  function showCategory($connect){
  $query_cat = "SELECT * FROM category ";
  $res_cat = mysqli_query($connect, $query_cat);
  if (!$res_cat){
    die ('Failed to retrieve information showCategories : ' . mysqli_error($res_cat));
  } else {
    while ($row_cat = mysqli_fetch_assoc($res_cat)){
      $id_cat = $row_cat['cat_id'];
      $title_cat = $row_cat['cat_title'];
      echo "
      <tr class='info'>
        <td>{$id_cat}</td>
        <td>{$title_cat}</td>
        <td><button class='btn btn-danger' style='background-color:red;' class='text-center' ><a  href='category.php?delete={$id_cat}'><span style='color:white'>Delete</span></a></button></td>
        <td><button class='btn btn-info' style='background-color:yellow;' class='text-center'><a href='category.php?edit={$id_cat}'><span style='color:green'>Edit</span></a></button></td>
      </tr>";
    }
  }
}
?>

<!-- Delete categorires -->
<?php
  function deleteCategory($connect){
    if (isset($_GET['delete'])){
      $cat_id = $_GET['delete'];
      $query_delete = "DELETE FROM category  WHERE cat_id='{$cat_id}' ";
      $rest_delete = mysqli_query($connect, $query_delete);
      echo "<script>
            window.location.href = 'category.php';
            </script>";
      if (!$rest_delete){
        die ('Failed to execute query' . mysqli_error($rest_delete));
      }
    }
  }
 ?>

 <!-- Retreive data to edit -->

 <?php
  function getEdit($connect){
   if (isset($_GET['edit'])){
     $cat_id = $_GET['edit'];
     $query_edit = "SELECT * FROM category WHERE cat_id='{$cat_id}' ";
     $res_edit = mysqli_query($connect, $query_edit);
     if (!$res_edit){
       die ('Failed to execute query' . mysqli_error($res_edit));
     } else {
       $row = mysqli_fetch_assoc($res_edit);
       $cat_id = $row['cat_id'];
       $cat_title = $row['cat_title'];

       echo "<input type='text' class='' name='update_id' style='width:40px; background-color:black; border:3px solid grays; padding-left:4px; color:white' value='{$cat_id}' readonly> ";
       echo "<input type='text' class='form-control' name='update_title' value='{$cat_title}'> ";
       echo "<br>
         <div class='group-form'>
           <input type='submit' class='btn btn-success' name='update' value='Update category'>
           </div>";
     };
   };
}
  ?>

<!-- Update category -->
<?php
  function updateCat($connect){
    if (isset($_POST['update'])){
      $cat_title = $_POST['update_title'];
      $cat_title = filter_var($cat_title, FILTER_SANITIZE_STRING);
      $cat_id = $_POST['update_id'];
      $query_update = "UPDATE category SET cat_title='{$cat_title}' ";
      $query_update .= "WHERE cat_id={$cat_id} ";
      $res_update = mysqli_query($connect, $query_update);
      echo "<script>
            window.location.href = 'update_cat.php';
            </script>";
      if (!$res_update){
        die ('Failed to query' . mysqli_error($res_update));
      }
    }
  };
?>

<!-- View all posts -->
<?php
  function viewAllPosts($connect){
    echo " <form action='' method='post' class='form-group'>
            <div class='form-group'>
              <input type='submit' class='btn btn-danger' name='Delete_all_post' value='Delete All posts'>
            </div>
          </form>
          " ;
    echo"<table class='table table-bordered table-responsive table-hover'>
      <thead>
        <tr class='info'>
          <th>Id</th>
          <th>Author</th>
          <th>Title</th>
          <th>Category</th>
          <th>Status</th>
          <th>image</th>
          <th>Tags</th>
          <th>Conntent</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody>";
    $user_id_0 = $_SESSION['user_id'];
    $user_role_0 = $_SESSION['role'];
    if ($user_role_0 == 'Subscriber'){
    $query_display_post = "SELECT * FROM posts WHERE post_user_id={$user_id_0} ";
  } else {
    $query_display_post = "SELECT * FROM posts ";
  }
    $res_post = mysqli_query($connect, $query_display_post);
    if (!$res_post){
      die('Check Failed to query' . mysqli_error($connect));
    } else {
      while ($row = mysqli_fetch_assoc($res_post)){
        $post_id = $row['post_id'];
        $post_cat_id = $row['post_category_id'];
        $post_title = $row['post_title'];
        $post_author = $row['post_author'];
        $post_date = $row['post_date'];
        $post_tag = $row['post_tags'];
        $post_image = $row['post_image'];
        $post_content = substr($row['post_content'],0,30);
        $post_count = $row['post_comment_count'];
        $post_status = $row['post_status'];
        $post_views_count = $row['post_views_count'];
        echo "<tr class='success'>";
        echo "<td id='p_id'>{$post_id}</td>
        <td>{$post_author}</td>
        <td>{$post_title}</td>";
          echo "<td>{$post_cat_id}</td>
          <td>{$post_status}</td>
          <td><img class='img-responsive' src='../image/{$post_image}'/></td>
          <td>{$post_tag}</td>
          <td>{$post_content}</td>
          <td>{$post_date}</td>
          <td><button class='btn btn-danger' style='background-color:red;' ><a href='posts.php?delete_post={$post_id}'><span style='color:white'>Delete</span></a></button></td>
          <td><button class='btn btn-info' style='background-color:yellow;'><a href='posts.php?source=edit%20post&&edit_post={$post_id}'><span style='color:green'>Edit</span></a></button></td>";
          echo "</tr>";
        }
      }
      echo "</tbody>
    </table>";
    }

    if (isset($_POST['Delete_all_post'])){
      $query = "DELETE FROM posts ";
      $res = mysqli_query($connect, $query);
      if (!$res){
        die ('Faile to delete all ' . mysqli_error($connect));
      }
    }

    if (isset($_GET['delete_post']))
    {
      $post_id = $_GET['delete_post'];
      $query = "DELETE FROM posts WHERE post_id={$post_id} ";
      $res_del_post = mysqli_query($connect, $query);
      if (!$res_del_post){
        die('Failed to query: ' . mysqli_error($connect));
      }
      echo "<script>
      window.location.href = 'posts.php';
      </script>";
    }

 ?>

<!-- View all comments -->

<?php
  function viewAllComments($connect, $source, $role){
    echo " <form action='' method='post' class='form-group'>
            <div class='form-group'>
              <input type='submit' class='btn btn-danger' name='Delete_all_com' value='Delete All comments'>
            </div>
          </form>
          " ;
    echo"<table class='table table-bordered table-responsive table-hover'>
      <thead>
        <tr class='info'>
          <th>Id</th>
          <th>Author</th>
          <th>Comment</th>
          <th>Email</th>
          <th>Status</th>
          <th>In response to</th>
          <th>Date</th>";
          if ($role == 'a'){
          echo "<th>Approve</th>
          <th>Unapprove</th>";
        };
          echo"<th>Delete</th>
        </tr>
      </thead>
      <tbody>";
      if ($role == 'a')
      $query_display_comments = "SELECT * FROM comments ";
      else if ($role == 's')
      $query_display_comments = "SELECT * FROM comments WHERE comment_user_id={$source} ";
    $res_comments = mysqli_query($connect, $query_display_comments);
    if (!$res_comments){
      die('Check Failed to query' . mysqli_error($connect));
    } else {
        $rl = $_GET['rl'];
        $id = $_GET['id'];
      while ($row = mysqli_fetch_assoc($res_comments)){
        $c_id = $row['comment_id'];
        $c_post_id = $row['comment_post_id'];
        $c_author = $row['comment_author'];
        $c_date = $row['comment_date'];
        $c_email = $row['comment_email'];
        $c_content = substr($row['comment_content'],0,30);
        $c_status = $row['comment_status'];
        echo "<tr class='success'>";
        echo "<td>{$c_id}</td>
          <td>{$c_author}</td>
          <td>{$c_content}</td>
          <td>{$c_email}</td>
          <td>{$c_status}</td>
          <td> ......... </td>
          <td>{$c_date}</td>";
          if ($role == 'a'){
          echo "<td><button class='btn btn-info' style='background-color:yellow;'><a href='comments.php?rl={$rl}&&id={$id}&&p_id={$c_post_id}&&approve={$c_id}'><span style='color:green'>Approve</span></a></buton></td>
          <td><button class='btn btn-info' style='background-color:yellow;'><a href='comments.php?rl={$rl}&&id={$id}&&p_id={$c_post_id}&&unapprove={$c_id}'><span style='color:green'>Unapprove</span></a></buton></td>";
        };
          echo "<td><button class='btn btn-danger' style='background-color:red;' ><a href='comments.php?rl={$rl}&&id={$id}&&delete_comment={$c_id}'><span style='color:white'>Delete</span></a></buton></td>";
            echo "</tr>";
        }
      }
      echo "</tbody>
    </table>";
    }

    if (isset($_POST['Delete_all_com'])){
      $u_id = $_GET['id'];
      $role = $_GET['rl'];
      if ($role == 'a')
      $query = "DELETE FROM comments ";
      else {
        $query = "DELETE FROM comments WHERE comment_user_id={$u_id} ";
      }
      $res = mysqli_query($connect, $query);
      if (!$res){
        die ('Faile to delete all ' . mysqli_error($connect));
      }
    }

    if (isset($_GET['delete_comment']))
    {
      $id = $_GET['id'];
      $rl = $_GET['rl'];
      $c_id = $_GET['delete_comment'];
      $query = "DELETE FROM comments WHERE comment_id={$c_id} ";
      $res_del_comment = mysqli_query($connect, $query);
      if (!$res_del_comment){
        die('Failed to query: ' . mysqli_error($connect));
      }
      echo "<script>
      window.location.href = 'comments.php?rl={$rl}&&id={$id}&&com=1';
      </script>";
    }
 ?>

 <?php

  if (isset($_GET['unapprove'])){
    $c_id = $_GET['unapprove'];
    $rl = $_GET['rl'];
    $id = $_GET['id'];
    $query = "UPDATE comments SET comment_status='Unapproved' WHERE comment_id={$c_id} ";
    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to appove : ' . mysqli_error($connect));
    }
    echo "<script>
    window.location.href = 'comments.php?rl={$rl}&&id={$id}&&com=1' ;
    </script>";
  }

 if (isset($_GET['approve'])){
   $c_id = $_GET['approve'];
   $rl = $_GET['rl'];
   $id = $_GET['id'];
   $query = "UPDATE comments SET comment_status='Approved' ";
   $query .= "WHERE comment_id={$c_id} ";
   $res = mysqli_query($connect, $query);
   if (!$res){
     die ('Failed to appove : ' . mysqli_error($connect));
   }
   $query = "UPDATE comments SET comment_status='Approved' WHERE comment_id={$c_id} ";
   $res = mysqli_query($connect, $query);
   if (!$res){
     die ('Failed to appove : ' . mysqli_error($connect));
   }
   echo "<script>
   window.location.href = 'comments.php?rl={$rl}&&id={$id}&&com=1' ;
   </script>";
 }
?>

<!-- Show comments -->

<?php
function approveComments($connect, $p_id){
    $query = "SELECT * FROM comments WHERE comment_post_id={$p_id} ";
    $res = mysqli_query($connect, $query);
    if (!$res){
      die (':Failed to approve ' . mysqli_error($connect));
    } else  {
      while ($row = mysqli_fetch_assoc($res)){
        $c_status = $row['comment_status'];
        $c_author = $row['comment_author'];
        $c_date = $row['comment_date'];
        $c_cont = $row['comment_content'];
        $c_content = chunk_split($c_cont, 100, "\n");
        if ($c_status == 'Approved') {
          echo "
          <div class='media'>
            <div class='media-left media-top'>
              <img src='http://placehold.it/64x64' class='media-object' style='width:80px'>
            </div>
            <div class='media-body'>
              <h4 class='media-heading'>{$c_author}   <small>{$c_date}</small></h4>
              <div><p>{$c_content}</p></div>
              </div>
          </div>";
          }
        }
      }
    }
?>

<!-- Get comments -->

<?php
function getComment($connect, $p_id, $c_user_id){
  if (isset($_POST['c_submit'])){
      $c_post_id = $p_id;
      $c_author = $_POST['c_author'];
      if ($c_user_id == '')
        $c_user_id = 0;
      $c_author = filter_var($c_author, FILTER_SANITIZE_STRING);
      $c_email = $_POST['c_email'];
      $c_email = filter_var($c_email, FILTER_SANITIZE_STRING);
      $c_content = $_POST['c_content'];
      $c_content = filter_var($c_content, FILTER_SANITIZE_STRING);
    $query = "INSERT INTO comments(comment_post_id, comment_user_id, comment_author, comment_email, comment_content, comment_status, comment_date) ";
    $query .= "VALUES ({$c_post_id}, {$c_user_id}, '{$c_author}', '{$c_email}', '{$c_content}', 'Approved', now()) ";
    $res = mysqli_query($connect, $query);
    if (!$res){
      die (':Failed to query fill comment. : ' . mysqli_error($connect));
    } else {
      $check = 1;
      return $check;
    }
  }
}
?>

<!-- Error login message -->
<?php
function errorMsg(){
  if (isset($_GET['error'])){
  $chek =  $_GET['error'];
  if ($chek == 1){
    echo "
    <script type='text/javascript'>
    $(document).ready(function(){
        $().tostie({type:'error', message:'Invalid login or password, please try again!'});
        return false;
    });
    </script> ";
    }
  }
}
?>

<!-- View all Users -->
<?php
  function viewAllUsers($connect){
    echo " <form action='' method='post' class='form-group'>
            <div class='form-group'>
              <input type='submit' class='btn btn-danger' name='Delete_all' value='Delete All users'>
            </div>
          </form>
          " ;
    echo "<div class='table-responsive'>";
    echo"<table class='table table-bordered table-hover'>
      <thead>
        <tr class='info'>
          <th>Id</th>
          <th>username</th>
          <th>password</th>
          <th>Email</th>
          <th>Firstname</th>
          <th>lastname</th>
          <th>image</th>
          <th>Role</th>
          <th>Admin</th>
          <th>Subscriber</th>
          <th>Edit</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>";
    $query_display_post = "SELECT * FROM USERS ";
    $res_post = mysqli_query($connect, $query_display_post);
    if (!$res_post){
      die('Check Failed to query' . mysqli_error($res_post));
    } else {
      while ($row = mysqli_fetch_assoc($res_post)){
        $user_id = $row['user_id'];
        $username = $row['username'];
        $password = $row['user_password'];
        $password = mysqli_real_escape_string($connect, $password);
        $email = $row['email'];
        $firstname = $row['firstname'];
        $lastname = $row['lastname'];
        $image = $row['image'];
        $role = $row['role'];
        echo "<tr class='success'>";
        echo "<td>{$user_id}</td>
        <td>{$username}</td>
        <td>{$password}</td>
        <td>{$email}</td>";
          echo "<td>{$firstname}</td>
          <td>{$lastname}</td>
          <td><img class='img-responsive' src='../image/{$image}'/></td>
          <td required>{$role}</td>
          <td><button class='btn btn-primary' style='background-color:yellow;'><a href='users.php?admin={$user_id}'><span style='color:green'>Admin</span></a></td>
          <td><button class='btn btn-primary' style='background-color:yellow;'><a href='users.php?subscriber={$user_id}'><span style='color:green'>Subscriber</span></a></td>
          <td><button class='btn btn-info' style='background-color:yellow;'><a href='users.php?source=edit users&&user_id={$user_id}'><span style='color:green'>Edit</span></a></td>
          <td><button class='btn btn-danger' style='background-color:red;' ><a href='users.php?delete_user={$user_id}'><span style='color:white'>Delete</span></a></td>";
            echo "</tr>";
        }
      }
      echo "</tbody>
    </table>
    </div>";
    }

      $admin_id = $_SESSION['user_id'];
      $admin_name = $_SESSION['username'];
      if (isset($_POST['Delete_all'])){
      $query = "DELETE FROM USERS WHERE user_id NOT IN ($admin_id) ";
      $res = mysqli_query($connect, $query);
      if (!$res){
        die ('Faile to delete all ' . mysqli_error($connect));
      }
    }

    if (isset($_GET['delete_user']))
    {
      $user_id = $_GET['delete_user'];
      if ($user_id == $admin_id)
        echo "<script type='text/javascript'>
          alert('You are not allowed to delete the current [$admin_name] account')
        </script> ";
      else {
      $query = "DELETE FROM USERS WHERE user_id={$user_id} ";
      $res_del_post = mysqli_query($connect, $query);
      if (!$res_del_post){
        die('Failed to query: ' . mysqli_error($connect));
      }
      echo "<script>
      window.location.href = 'users.php';
      </script>";
      }
    }
 ?>

<!-- Approve/unaprove users -->

<?php

 if (isset($_GET['subscriber'])){
   $c_id = $_GET['subscriber'];
   $query = "UPDATE USERS SET role='Subscriber' WHERE user_id={$c_id} ";
   $res = mysqli_query($connect, $query);
   if (!$res){
     die ('Failed to appove : ' . mysqli_error($connect));
   }
 }

if (isset($_GET['admin'])){
  $c_id = $_GET['admin'];
  $query = "UPDATE USERS SET role='Admin' ";
  $query .= "WHERE user_id={$c_id} ";
  $res = mysqli_query($connect, $query);
  if (!$res){
    die ('Failed to appove : ' . mysqli_error($connect));
  }
}
?>

<!-- Count posts for dashbaord -->
<?php
function countPost($connect){
    $query = "SELECT * FROM posts ";
    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to count posts . ' . mysqli_error($connect));
    }
    $count = mysqli_num_rows($res);
    echo $count;
}

?>

<!-- Count comments for dashbaord -->
<?php
function countComments($connect){
    if (isset($_SESSION['role'])){
      $user_role = $_SESSION['role'];
      $user_id = $_SESSION['user_id'];
      if ($user_role == 'Admin'){
    $query = "SELECT * FROM comments ";
    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to count comments admin . ' . mysqli_error($connect));
    }
    $count = mysqli_num_rows($res);
    echo $count;
    } else if ($user_role == 'Subscriber'){
      $query = "SELECT * FROM comments WHERE comment_user_id={$user_id} ";
      $res = mysqli_query($connect, $query);
      if (!$res){
        die ('Failed to count comments USER. ' . mysqli_error($connect));
      }
      $count = mysqli_num_rows($res);
      echo $count;
    }
  }
}
?>

<!-- Count users for dashbaord -->
<?php
function countUsers($connect){
    $query = "SELECT * FROM USERS ";
    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to count users . ' . mysqli_error($connect));
    }
    $count = mysqli_num_rows($res);
    echo $count;
}
?>

<!-- Count users for dashbaord -->
<?php
function countCategory($connect){
    $query = "SELECT * FROM category ";
    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Failed to count category . ' . mysqli_error($connect));
    }
    $count = mysqli_num_rows($res);
    echo $count;
}
?>

<!-- Show name -->
<?php
function showName($connect){
    if (isset($_SESSION['firstname'])){
    $u_id = $_SESSION['user_id'];
    $query = "SELECT * FROM USERS WHERE user_id='{$u_id}' ";
    $res = mysqli_query($connect, $query);
    $row = mysqli_fetch_assoc($res);
    $username = $row['username'];
    echo $username ;
  }
}
?>


<!-- Delete all categoris -->
<?php
function DelAllCat($connect){
  if (isset($_POST['Delete_all_cat'])){
    $query = "DELETE FROM category ";
    $res = mysqli_query($connect, $query);
    if (!$res){
      die ('Faile to delete all ' . mysqli_error($connect));
    }
  }
}
 ?>
 <!-- Count rows -->

 <?php
 function countRow($connect){
  if ($_SESSION['role'] == 'Admin'){
 $query = "SELECT * FROM posts WHERE post_status='Published' ";
 } else if ($_SESSION['role'] == 'Subscriber'){
   $user_p_id = $_SESSION['user_id'];
   $query = "SELECT * FROM posts WHERE post_status='Published' AND  post_user_id={$user_p_id} ";
 } else {
   $query = "SELECT * FROM posts WHERE post_status='Published' ";
 }
 $res = mysqli_query($connect, $query);
 if (!$res)
 {
   die ('Failed to count rows ' . mysqli_error($connect));
 }
 $count = mysqli_num_rows($res);
 return $count;

}
 ?>
