<?php function redirPost(){ header('location: posts.php'); }; ?>
<?php include "../includes/db.php" ?>
<?php
if (isset($_GET['edit_post']))
{
  $p_id = $_GET['edit_post'];
  $query = "SELECT * FROM posts WHERE post_id='{$p_id}' ";
  $rest = mysqli_query($connect, $query);
  if (!$rest){
    die ('Failed to edit posts ' . mysqli_error($connect));
  } else {
    while ($row = mysqli_fetch_assoc($rest)){
      $p_id = $row['post_id'];
      $p_cat_id = $row['post_category_id'];
      $p_cat_id = filter_var($p_cat_id, FILTER_SANITIZE_STRING);
      $p_title = $row['post_title'];
      $p_title = filter_var($p_title, FILTER_SANITIZE_STRING);
      $p_author = $row['post_author'];
      $p_author = filter_var($p_author, FILTER_SANITIZE_STRING);
      $p_date = $row['post_date'];
      $p_image = $row['post_image'];
      $p_content = $row['post_content'];
      $p_tags = $row['post_tags'];
      $p_tags = filter_var($p_tags, FILTER_SANITIZE_STRING);
      $p_status = $row['post_status'];
      $p_status = filter_var($p_status, FILTER_SANITIZE_STRING);
    }
  }
}
  if (isset($_POST['update'])){
      $p_id = $_GET['edit_post'];;
      $p_title = $_POST['title'];
      $p_title = filter_var($p_title, FILTER_SANITIZE_STRING);
      $p_cat = $_POST['post_category_id'];
      $p_cat = filter_var($p_cat, FILTER_SANITIZE_STRING);
      $query_c = "SELECT cat_id FROM category WHERE cat_title='{$p_cat}' ";
      $res_c = mysqli_query($connect, $query_c);
      $row = mysqli_fetch_assoc($res_c);
      $p_cat_id = $row['cat_id'];
      $p_author = $_POST['post_author'];
      $p_author = filter_var($p_author, FILTER_SANITIZE_STRING);
      $p_status = $_POST['post_status'];
      $p_status = filter_var($p_status, FILTER_SANITIZE_STRING);
      $p_img = $_FILES['post_img']['name'];
      $p_img_dir = $_FILES['post_img']['tmp_name'];
      $p_tags = $_POST['post_tags'];
      $p_tags = filter_var($p_tags, FILTER_SANITIZE_STRING);
      $p_content = $_POST['post_content'];
      $p_content = utf8_encode($p_content);
      $p_content = chunk_split($p_content, 100, '\n');
      $p_date = date('d-m-y');
      $p_comments_count = 4;
      $p_views_count = 4;
      if(is_dir('images/')) {
      move_uploaded_file($p_img_dir, "../image/$p_img");
      } else {
      echo 'Le dossier n\'existe pas';
      echo "please create : '../inage/' ";
      }
     $query_p_insert = "UPDATE posts SET post_category_id={$p_cat_id}, post_title='{$p_title}', post_author='{$p_author}', post_status='{$p_status}', post_date=now(), post_image='{$p_img}', post_content='{$p_content}', post_tags='{$p_tags}' ";
     $query_p_insert .= "WHERE post_id={$p_id} ";
     $res_p_insert = mysqli_query($connect, $query_p_insert);
     $value = 1;
     if (!$res_p_insert){
       die ('Failed to insert data' . mysqli_error($connect));
       $valid = 0;
     }  else
      $valid = 1;
  }
 ?>
 <?php if ($valid == 1)
 echo "<p style='color:green'><b>Post has been updatted successfully</b> <a href='posts.php'> View posts</a></p>";
 ?>
<!-- Form structure -->
<form action="" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="title">Post title</labl>
    <input type="text" class="form-control" value="<?php echo $p_title ; ?>" name="title">
  </div>

  <div class="form-group">
    <label for="post_category">Post category</labl><br>
      <select value="" name="post_category_id">
        <?php
          $query = "SELECT * FROM category ";
          $res = mysqli_query($connect, $query);
          if (!$res){
            die (':Failed to query: ' . mysqli_error($connect));
          } else {
          while ($row = mysqli_fetch_assoc($res)){
            $cat_title = $row['cat_title'];
            echo "<option value='{$cat_title}'>{$cat_title}</option>";
          }
        }
        ?>
      </select>
    </div>

  <div class="form-group">
    <label for="post_author">Post author</labl>
    <input type="text" class="form-control" value="<?php echo $p_author ; ?>" name="post_author">
  </div>

  <div class="form-group">
    <label>Post status</label><br>
    <select value="" name="post_status">
        <option value="Draft">Draft</option>;
        <option value="Published">Published</option>;
    </select>
  </div>

  <div class="form-group">
    <label for="post_img">Post image</labl><br>
    <img style="width:100px;" src="<?php echo "../image/$p_image" ?>" /><br>
    <br>
    <input type="file"  value="<?php echo $p_image ; ?>" name="post_img" required>
  </div>

  <div class="form-group">
    <label for="post_tags">Post tags</labl>
    <input type="text" class="form-control" value="<?php echo $p_tags ; ?>" name="post_tags">
  </div>

  <div class="form-group">
    <label for="Post content">Post content</labl>
    <textarea type="text" class="form-control"  rows=10 cols=30  name="post_content"><?php echo $p_content ; ?> </textarea>
  </div>

  <div class="form-group">
    <input type="submit" class="btn btn-success" name="update" value="Update posts">
    <button class="btn btn-primary" href=""><a href="posts.php" style="color:white; text-decoration:none;">Cancel</a></button>
  </div>
</form>
