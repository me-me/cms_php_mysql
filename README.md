# README #

A CMS (Content Management System) blog website. 

### What is this repository for? ###

This repo contains the source code of the postYbe website.

##Features:
##A frontend UX interface :
	1. Responsive website.
	2. Login system
	3. Registration system [on developement]
	4. Search blog feature
	5. Comments option
	6. Pagings
	7. Friendly error pop ups
##A back end admin CMS:
	1. Dashboard analytics
	2. Posts management
		2.1 View posts
		2.2 add, delete and edit posts
	3. Category management:
		3.1 Add, view, edit and delete Category
	4. Users management:
		4.1 Add, view, edit and delete users
	5. User profile
		5.1 Edit profile

### How do I get set up? ###
	Setup the website.
	Requirement:
		  A hosting service with PHP7.x and MySql.
	
	Setup:
		1. Clone the repo : git clone https://me me@bitbucket.org/me me/cms_php_mysql.git
		2. Go to /includes/db.php
		3. Setup your database connection credentials:  username, and password
	
	Once you are done, host the website. the database will be created automatically.
	and an Admin user will be created with credentials: username:admin   |    password:admin


### Who do I talk to? ###
	  This is a beta version developed with Html/css/js/Bootstrap/Jquery/PHP/mysql
	If you have any questions contact: abnaceur@student.42.fr
		oe contact@naceur abdeljalil.com
