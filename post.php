<?php include "admin/includes/functions.php" ?>
<!-- Header -->
<?php include "includes/header.php" ?>
    <!-- Navigation -->
  <?php include "includes/navigation.php" ?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div id="post_not" class="col-lg-8">

                <!-- Blog Post -->
                <?php postId($connect, $p_id); ?>
                <?php getCategory($connect,   $cat_id); ?>
                <!-- Blog Comments -->
                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form action="" method="post" role="form">
                      <label>Author:</label>
                      <div class="form-group">
                          <input class="form-control" name="c_author"  required>
                      </div>

                      <label>Email:</label>
                      <div class="form-group">
                        <input type="email" class="form-control" name="c_email" required>
                      </div>

                        <div class="form-group">
                          <label>Merssage:</label>
                            <textarea class="form-control" name="c_content" rows="3" required></textarea>
                        </div>
                        <button type="submit" id="simple-notice"  class="btn btn-primary" name="c_submit">Submit</button>
                    </form>
                </div>
                <hr>
                <!-- Posted Comments -->
                <?php
                if (isset($_GET['p_id'])){
                  //if (isset($_GET['u_id'])){
                  $p_id = $_GET['p_id'];
                  $c_user_id = $_GET['u_id'];
                  $checker = 0;
                  $checker = getComment($connect, $p_id, $c_user_id);
                  if ($checker == 1) {
                    echo "
                    <script type='text/javascript'>
                    $(document).ready(function(){
                        $().tostie({type:'notice', message:'Message has been submited successfully. Waiting for approval.'});
                        return false;
                    });
                    </script> ";
                  }
                  approveComments($connect, $p_id);
                }
              //}
                  ?>
            </div>
              <?php include "includes/sidebar.php" ?>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; postYbe 2017 dev by A. Naceur</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->
    <!-- footer -->
  <?php include "includes/footer.php" ?>
    <!-- End footer -->
