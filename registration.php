<?php  include "includes/db.php"; ?>
 <?php  include "includes/header.php"; ?>


    <!-- Navigation -->

    <?php  include "includes/navigation.php"; ?>


    <!-- Page Content -->
    <div class="container">

<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">
<div class="well">
                <div class="form-wrap">
                <h3>Register</h3><br>
                <?php if (isset($_GET['empty'])){
                  $value = $_GET['empty'];
                  if ($value == 1) {
                  echo "
                  <script type='text/javascript'>
                  $(document).ready(function(){
                      $().tostie({type:'success', message:'Registration has been submited successfully. Waiting for approval.'});
                      return false;
                  });
                  </script> ";
                } else if ($value == 0) {
                  echo "
                  <script type='text/javascript'>
                  $(document).ready(function(){
                      $().tostie({type:'error', message:'Fields should not be empty.'});
                      return false;
                  });
                  </script> ";
                }}?>
                    <form role="form" action="includes/register.php" method="post" id="login-form" autocomplete="off">
                        <div class="form-group">
                            <label for="username" class="sr-only">username</label>
                            <input type="text" name="username" id="username" class="form-control" placeholder="Enter Desired Username" >
                        </div>
                         <div class="form-group">
                            <label for="email" class="sr-only">Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="somebody@example.com" >
                        </div>
                         <div class="form-group">
                            <label for="password" class="sr-only">Password</label>
                            <input type="password" name="password" id="key" class="form-control" placeholder="Password" >
                        </div>

                        <input type="submit" name="submit_register" id="btn-login" class="btn btn-primary btn-lg btn-block" value="Register"  >
                    </form>

                </div>
              </div>
            </div> <!-- /.col-xs-12 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section>
        <hr>
<?php include "includes/footer.php";?>
